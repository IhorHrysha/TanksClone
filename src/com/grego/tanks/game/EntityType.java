package com.grego.tanks.game;

public enum EntityType {
		
	Player,
	Enemy,
	Tile,
	Eagle,
	Border,
	Explosion,
	Bullet,
	Bonus;
	
}
