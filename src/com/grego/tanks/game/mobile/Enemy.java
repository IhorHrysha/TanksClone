package com.grego.tanks.game.mobile;

import java.util.Random;

import com.grego.tanks.game.EntityType;
import com.grego.tanks.game.Heading;
import com.grego.tanks.main.Handler;
import com.grego.tanks.utils.Time;

public class Enemy extends Tank {

	public static final int GAME_CYCLE = 256; 
	public static final int SHOOT_POSIBILITY = 32;
	
	
	//private long
	int ticksPerCycle;
	int respawnPerionDuration;
		
	public Enemy(Handler handler, float x, float y, int starsCount) {
				
		super(EntityType.Enemy, handler, x, y, true, starsCount, TankColor.grey);
		
		setHeading(Heading.SOUTH);
		
	
		ticksPerCycle = 0;
		
		respawnPerionDuration = getRespawnPeriodDuration();
		
	}

	@Override
	public void getInput() {
		
		
		
	}
	
	public void update() {
			
		super.update();
		
		int gamePeriodDuration = respawnPerionDuration * 8;
					
		if (ticksPerCycle<gamePeriodDuration) {
			//random movement
		} else if (ticksPerCycle<gamePeriodDuration*2) {
			//player killer
			moveToPlayer();
		} else {
			//eagle killer
			
			moveToEagle();
			
			if(ticksPerCycle>=GAME_CYCLE*60) {
				ticksPerCycle = 0;
			}
		}
		
		ticksPerCycle++;				
	}
	
	public void moveToEagle() {
		
	}
	
	public void moveToPlayer() {
		handler.getEntityManager();
	}
	
	public void moveSetRandomDirection() {
		
	}
	
	//in ticks
	private int getRespawnPeriodDuration(){
				
		//TODO get data from env
		int level = 1;
		int player_count = 1;
		
		return 190 - level * 4 - (player_count - 1) * 20;
	}

	@Override
	public Bullet makeShoot() {
		
		Random rnd = new Random();
		
		if (rnd.nextInt(SHOOT_POSIBILITY)==0 && (bulletsList.isEmpty() || Time.get() - lastShoot > (long) (Time.SECOND * shootRate))) {

			return createBullet();

		}
		
		return null;
	}

}
