package com.grego.tanks.game.mobile;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.grego.tanks.game.EntityType;
import com.grego.tanks.game.Heading;
import com.grego.tanks.graphics.Animation;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.main.Handler;
import com.grego.tanks.utils.Time;


public abstract class Tank extends MobileEntity {
	
	public static final int DEFAULF_SHOOT_RATE = 1;
			
	//shoot system
	protected long lastShoot;
	protected double shootRate;
	
	//tank type
	protected boolean isEnemy;
	protected int starsCount;
	protected TankColor color;
	
	//animation
	protected Animation animation; 
	
	protected ArrayList<Bullet> bulletsList = new ArrayList<>();
	
	public Tank(EntityType type, Handler handler, float x, float y, boolean isEnemy, int starsCount, TankColor color) {
		super(type, handler, x, y, handler.getGame().getTankSize()*handler.getGame().getScale(), handler.getGame().getTankSize()*handler.getGame().getScale());
							
		this.shootRate = DEFAULF_SHOOT_RATE;
		this.lastShoot = Time.get()-(long)(Time.SECOND*shootRate);
				
		this.color = color;
		this.starsCount = starsCount;
		this.isEnemy = isEnemy;
		
		animation = new Animation(250, getAnimationFrames());
			
	}

	private BufferedImage[] getAnimationFrames() {
		
		int frameCount = 2;
		
		BufferedImage[] frames = new BufferedImage[frameCount];
		
		for (int i = 0; i < frameCount; i++)
			frames[i] = Assets.getTankTexture(this,i);
					
		return frames;
	}
	
	
	public abstract void getInput();

	
	private float normalizePosition(float pos) {
		int scaledTile = handler.getGame().getTileSize()*scale;
		
		int posInFullTiles = (int)(pos/scaledTile);
		
		if (pos%scaledTile>scaledTile/2)
			posInFullTiles++;
		
		return posInFullTiles*scaledTile;
	}
	
	
	
	public abstract Bullet makeShoot();
	
	protected Bullet createBullet() {

		float gunShiftX = 0;
		float gunShiftY = 0;

		Bullet bullet = new Bullet(handler, x, y, this, 8);

		// bullet centered for gun
		if (heading == Heading.NORTH) {
			gunShiftX = width / 2 - bullet.getWidth() / 2;
			gunShiftY = -bullet.getHeight();
		} else if (heading == Heading.SOUTH) {
			gunShiftX = width / 2 - bullet.getWidth() / 2;
			gunShiftY = height;
		} else if (heading == Heading.WEST) {
			gunShiftX = -bullet.getWidth();
			gunShiftY = height / 2 - bullet.getHeight() / 2;
		} else if (heading == Heading.EAST) {
			gunShiftX = width;
			gunShiftY = height / 2 - bullet.getHeight() / 2;
		}

		bullet.setX(x + gunShiftX);
		bullet.setY(y + gunShiftY);

		// create bullet
		bulletsList.add(bullet);
		lastShoot = Time.get();

		return bullet;

	}
	
	@Override
	public void update() {
		
		getInput();
				
		move();
			
		//play animation when tank moves
		//if(isMoving) {
			animation.update();
		//}
		
		sprite = animation.getCurrentFrame();
			
		//makeShoot();
		
	}


	public void removeBullet(Bullet bullet) {
		//from all bullets array 
		bulletsList.remove(bullet);
	}
		
	
	//GETTERS & SETTERS
		
	public void setHeading(Heading heading) {
		
		boolean changeFrames = false;
		
		if (this.heading != null) {

			// align when axis is changed
			if (heading.isVertical() != this.heading.isVertical()) {

				if (this.heading.isVertical())
					yMove = normalizePosition(y) - y;

				if (this.heading.isHorisontal())
					xMove = normalizePosition(x) - x;

			}

			// change animation frames
			changeFrames = !heading.equals(this.heading); 
			
		}
		
		this.heading = heading;
		
		if(changeFrames)
			animation.setFrames(getAnimationFrames());
	}

	public double getShootRate() {
		return shootRate;
	}

	public void setShootRate(double shootRate) {
		this.shootRate = shootRate;
	}

	public boolean isEnemy() {
		return isEnemy;
	}

	public void setEnemy(boolean isEnemy) {
		this.isEnemy = isEnemy;
	}

	public int getStarsCount() {
		return starsCount;
	}

	public void setStarsCount(int starsCount) {
		this.starsCount = starsCount;
	}

	public TankColor getColor() {
		return color;
	}

	public void setColor(TankColor color) {
		this.color = color;
	}
	
	

}
