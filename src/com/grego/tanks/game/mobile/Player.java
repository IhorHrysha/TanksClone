package com.grego.tanks.game.mobile;

import java.awt.event.KeyEvent;

import com.grego.tanks.game.EntityType;
import com.grego.tanks.game.Heading;
import com.grego.tanks.main.Handler;
import com.grego.tanks.utils.Time;

public class Player extends Tank {

	public static int FIRST = 1;
	public static int SECOND = 2;
	
	private int playerNumber;
	protected boolean shootKeyPressed;
	
	public Player(Handler handler, int playerNumber) {
		
		super(EntityType.Player, handler, 
				(playerNumber==FIRST?5:9)*handler.getGame().getTankSize()*handler.getGame().getScale(), 
				13*handler.getGame().getTankSize()*handler.getGame().getScale(), 
				false, 
				0, 
				(playerNumber==FIRST?TankColor.yellow:TankColor.green));
		
		this.playerNumber = playerNumber;
		this.shootKeyPressed = false;
	}

	@Override
	public void getInput() {

		xMove = 0;
		yMove = 0;

		if (playerNumber == FIRST) {

			if (handler.getInput().getKey(KeyEvent.VK_UP)) {
				setHeading(Heading.NORTH);
				setyMove(-speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_DOWN)) {
				setHeading(Heading.SOUTH);
				setyMove(speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_LEFT)) {
				setHeading(Heading.WEST);
				setxMove(-speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_RIGHT)) {
				setHeading(Heading.EAST);
				setxMove(speed);
			}

			shootKeyPressed = handler.getInput().getKey(KeyEvent.VK_NUMPAD0);
			
		} else {

			if (handler.getInput().getKey(KeyEvent.VK_W)) {
				setHeading(Heading.NORTH);
				setyMove(-speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_S)) {
				setHeading(Heading.SOUTH);
				setyMove(speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_A)) {
				setHeading(Heading.WEST);
				setxMove(-speed);
			} else if (handler.getInput().getKey(KeyEvent.VK_D)) {
				setHeading(Heading.EAST);
				setxMove(speed);
			}

			shootKeyPressed = handler.getInput().getKey(KeyEvent.VK_J);

		}

	}

	@Override
	public Bullet makeShoot() {

		if (shootKeyPressed && (bulletsList.isEmpty() || Time.get() - lastShoot > (long) (Time.SECOND * shootRate))) {

			return createBullet();

		}

		return null;
	}

}
