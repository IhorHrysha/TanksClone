package com.grego.tanks.game.mobile;

import java.awt.Point;

public enum TankColor {

	yellow(new Point(0,0)),
	green(new Point(0,8)),
	grey(new Point(8,0)),
	red(new Point(8,8));
	
	Point texturePosition;
	
	private TankColor(Point texturePosition) {
		this.texturePosition = texturePosition;
	}
	public Point getTexturePosition() {
		return texturePosition;
	}
}
