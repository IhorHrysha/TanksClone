package com.grego.tanks.game.mobile;

import com.grego.tanks.game.EntityType;
import com.grego.tanks.game.Heading;
import com.grego.tanks.graphics.Assets;

import com.grego.tanks.main.Handler;

public class Bullet extends MobileEntity {
		
	
	private Tank tank;
			
	public Bullet(Handler handler, float x, float y, Tank tank, float speed) {
		
		super(EntityType.Bullet, handler, x, y, 0, 0);
		
		int w,h;
		
		if (tank.getHeading().isHorisontal()){
			h = handler.getGame().getTankSize()*handler.getGame().getScale(); 
			w = handler.getGame().getTileSize()*handler.getGame().getScale();
		} else {
			w = handler.getGame().getTankSize()*handler.getGame().getScale(); 
			h = handler.getGame().getTileSize()*handler.getGame().getScale();
		}
		
		setWidth(w);
		setHeight(h);
		
		
		this.tank = tank;
		this.heading = tank.getHeading();
		sprite = Assets.bulletImageMap.get(heading);
				
		setSpeed(speed);
						
	}
	
	public void shift() {
		
		if(heading==Heading.NORTH) {
			yMove=-speed;
		} else if(heading==Heading.SOUTH){
			yMove=speed;
		} else if(heading==Heading.WEST){
			xMove=-speed;
		} else if(heading==Heading.EAST){
			xMove=speed;
		}
		
	}
	
	@Override
	public void update() {
			
		shift();
		
		move();
				
	}
	
	public Tank getTank() {
		return tank;
	}

}
