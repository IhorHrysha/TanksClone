package com.grego.tanks.game.mobile;

import com.grego.tanks.game.Entity;
import com.grego.tanks.game.EntityType;
import com.grego.tanks.game.Heading;
import com.grego.tanks.game.statics.Border;
import com.grego.tanks.game.statics.Eagle;
import com.grego.tanks.game.statics.Tile;
import com.grego.tanks.main.Handler;

public abstract class MobileEntity extends Entity {

	public static final float DEFAULT_SPEED = 3.0f;
	public static final Heading DEFAULT_HEADING = Heading.NORTH;
	
	//moving
	protected Heading heading;
	protected boolean isMoving;	
	protected float speed;
	protected float xMove, yMove;
		
	public MobileEntity(EntityType type, Handler handler, float x, float y, int width, int height) {
		super(type, handler, x, y, width, height);
		
		isMoving = false;
		heading = DEFAULT_HEADING;
		speed = DEFAULT_SPEED;
	}

	@Override
	public void update() {
		// TODO Auto-generated method stub

	}
	
	//moving
	public void move() {
		float tx = x, ty = y;
		
		if(!checkEntityCollisions(xMove, 0f))		
			moveX();
		if(!checkEntityCollisions(0f,yMove))
			moveY();
		
		//coords are changed
		isMoving = (tx!=x||ty!=y);
			
	}

	public void moveX() {x+=xMove;}
	
	public void moveY() {y+=yMove;}

	//collisions
	public boolean checkEntityCollisions(float xOffset, float yOffset) {
		
		//was deactivated by another entity
		if (!isActive())
			return false;
		
		boolean collisionDetected = false;
				
		for(Entity e:handler.getEntityManager().getEntities()) {

			if(e.equals(this) || e.isWalkable() || !e.isActive()) 
				continue;
						
			if(hasIntersection(e, xOffset, yOffset)) {
				
				System.out.println("Collision " + this + " with " + e);
				
				//bullet
				if (this.type==EntityType.Bullet) {
					
					//explosion
					//it.add(new Explosion(handler, entity.getX(), entity.getY()));
					
					//for bullets deactivate itself					
					if (e.getType() == EntityType.Bullet) {
												
						collisionDetected = collisionBulletBullet((Bullet)this, (Bullet)e);						
						
					} else if (e.getType() == EntityType.Player || e.getType() == EntityType.Enemy) {
						
						//kill enemy tank
						collisionDetected = collisionBulletTank((Bullet)this, (Tank)e);
						
					} else if (e.type == EntityType.Tile) {
						
						//destroy if its possible
						collisionDetected = collisionBulletTile((Bullet)this, (Tile)e);
						
					} else if (e.type == EntityType.Eagle) {
						
						collisionBulletEagle((Bullet)this, (Eagle)e);
						
					} else if (e.type == EntityType.Border) {
						
						collisionBulletBorder((Bullet)this, (Border)e);
						
					} else {
						continue;
					}
										
				} else if (this.type==EntityType.Enemy) {

					if (e.type == EntityType.Tile) {
						//for tank only stop moving on tiles(depends on tile type)
						return false;
					} else if (e.type == EntityType.Player || e.type == EntityType.Enemy || e.type == EntityType.Border) {
						//stop only
						return false;
					} else {
						continue;
					}
									
				} else if (this.type==EntityType.Player) {
					
					if (e.type == EntityType.Tile) {
						//for tank only stop moving on tiles(depends on tile type)
						return true;
					} else if (e.type == EntityType.Bonus) {
						//for player take bonuses, enemies walk throught
						
					} else if (e.type == EntityType.Player || e.type == EntityType.Enemy || e.type == EntityType.Border) {
						//stop only
						return true;
					} else {
						continue;
					}
									
				}
			}
		}
		
		return collisionDetected;
		
	}
	
	private boolean collisionBulletBullet(Bullet b, Bullet b2) {

		//friendly fire
		if (b.getTank().getType()==b2.getTank().getType())
			return false;

		
		b.setActive(false);
		b.getTank().removeBullet(b);
		
		b2.setActive(false);
		b2.getTank().removeBullet(b2);
				
		return true;
	}
	
	private boolean collisionBulletTank(Bullet b, Tank t) {
		
		//friendly fire
		if (b.getTank().getType()==t.getType())
			return false;

		b.setActive(false);
		b.getTank().removeBullet(b);
		
		t.setActive(false);
				
		return true;
	}
	
	private boolean collisionBulletTile(Bullet b, Tile t) {

		//TODO tile type dependence

		b.setActive(false);
		b.getTank().removeBullet(b);
		
		t.setActive(false);
				
		return true;
	}
	
	private void collisionBulletBorder(Bullet b, Border border) {

		b.setActive(false);
		b.getTank().removeBullet(b);
				
	}
	
	private void collisionBulletEagle(Bullet b, Eagle eagle) {

		b.setActive(false);
		b.getTank().removeBullet(b);
		
		eagle.setActive(false);
				
	}
	
	
	//getters setters
	public float getxMove() {
		return xMove;
	}

	public void setxMove(float xMove) {
		this.xMove = xMove;
	}

	public float getyMove() {
		return yMove;
	}

	public void setyMove(float yMove) {
		this.yMove = yMove;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public Heading getHeading() {
		return heading;
	}

	public void setHeading(Heading heading) {
		this.heading = heading;
	}

	public boolean isMoving() {
		return isMoving;
	}


}
