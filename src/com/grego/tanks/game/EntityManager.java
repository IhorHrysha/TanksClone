package com.grego.tanks.game;

import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.ListIterator;

import com.grego.tanks.game.mobile.Tank;
import com.grego.tanks.game.statics.Explosion;
import com.grego.tanks.main.Handler;


public class EntityManager {
	private Handler handler;
	private ArrayList<Entity> entities;
	private ArrayList<Explosion> explosions;

	public EntityManager(Handler handler) {
		this.handler = handler; 
		entities = new ArrayList<Entity>();
		explosions = new ArrayList<Explosion>();
	}

	public Handler getHandler() {
		return handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
	}

	public ArrayList<Entity> getEntities() {
		return entities;
	}

	public void setEntities(ArrayList<Entity> entities) {
		this.entities = entities;
	}

	public void addEntity(Entity e) {
		entities.add(e);
	}
	
	public void removeEntity(Entity e) {
		entities.remove(e);
	}

	public void addExplosion(Explosion exp) {
		explosions.add(exp);
	}
	
	public void update() {
		
		
		updateExplosions();
				
		updateEntities();	

	}
	
	private void updateExplosions() {
		
		
		ListIterator<Explosion> ite = explosions.listIterator();
		while (ite.hasNext()) {
			
			Entity explosion = ite.next();
			explosion.update();
		    
		    if (!explosion.isActive())
		    	ite.remove();	    			
		}

	}
	
	private void updateEntities() {
		
		ListIterator<Entity> it = entities.listIterator();
		while (it.hasNext()) {
			
			Entity entity = it.next();
			entity.update();
	    	
		    //make shoot
			if (entity.getType() == EntityType.Player || entity.getType() == EntityType.Enemy) {
				
				Entity bullet = ((Tank) entity).makeShoot();
				if (bullet != null)
					it.add(bullet);
			}
		    
		    if (!entity.isActive())
		    	it.remove();
		    			
		}	

	}

	
	public void render(Graphics2D g) {
		
		for (Entity entity : entities) {
			entity.render(g);
		}
		
		for (Entity explosion : explosions) {
			explosion.render(g);
		}
				
	}
	
}
