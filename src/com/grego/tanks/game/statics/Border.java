package com.grego.tanks.game.statics;

import java.awt.Graphics2D;

import com.grego.tanks.game.Entity;
import com.grego.tanks.game.EntityType;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.main.Handler;

public class Border extends Entity {

	public Border(Handler handler, float x, float y, int width, int height) {
		super(EntityType.Border, handler, x, y, width, height);
	}

	@Override
	public void update() {
		sprite = Assets.border;
	}


}
