package com.grego.tanks.game.statics;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import com.grego.tanks.game.Entity;
import com.grego.tanks.game.EntityType;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.graphics.SpriteSheet;
import com.grego.tanks.main.Handler;
import com.grego.tanks.utils.Utils;

public class Tile extends Entity{
	
	public static final TileType DEFAULT_TILE_TYPE = TileType.EMPTY; 
	
	private TileType tileType;
	
	public Tile(Handler handler, float x, float y) {
		super(EntityType.Tile, handler, x, y, 
				handler.getGame().getTileSize()*handler.getGame().getScale(), 
				handler.getGame().getTileSize()*handler.getGame().getScale());
		tileType = TileType.EMPTY;
	}
		

	@Override
	public void update() {
		//TODO init once
		//if (sprite != null)
			sprite = Assets.getTileTexture(tileType);
	}

	
	public TileType getTileType() {
		return tileType;
	}
	
	public void setTileType(TileType tileType) {
		this.tileType = tileType;
	}
	
}
