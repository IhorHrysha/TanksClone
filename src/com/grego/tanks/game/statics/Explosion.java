package com.grego.tanks.game.statics;

import com.grego.tanks.game.Entity;
import com.grego.tanks.game.EntityType;
import com.grego.tanks.graphics.Animation;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.main.Handler;

public class Explosion extends Entity {

	//animation
	protected Animation animation;
	
	public Explosion(Handler handler, float x, float y) {
		super(EntityType.Explosion, handler, x, y, 
				handler.getGame().getTankSize()*handler.getGame().getScale(), 
				handler.getGame().getTankSize()*handler.getGame().getScale());

		animation = new Animation(100, Assets.explosionFrames);
		
		setWalkable(true);

	}

	@Override
	public void update() {
		
		animation.update();
		sprite = animation.getCurrentFrame();
		if (animation.isPlayedOnce()) {
			active=false;
		}
		
	}

}
