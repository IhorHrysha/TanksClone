package com.grego.tanks.game.statics;

import java.awt.Graphics2D;

import javax.swing.plaf.SplitPaneUI;

import com.grego.tanks.game.Entity;
import com.grego.tanks.game.EntityType;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.main.Handler;



public class Eagle extends Entity {
		
	public Eagle(Handler handler, float x, float y) {
		super(EntityType.Eagle, handler, x, y, 
				handler.getGame().getTankSize()*handler.getGame().getScale(),
				handler.getGame().getTankSize()*handler.getGame().getScale());
		
	}


	@Override
	public void update() {
		sprite = (isActive()?Assets.eagleAlive:Assets.eagleDead);
	}


}
