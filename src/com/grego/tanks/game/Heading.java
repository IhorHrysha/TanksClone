package com.grego.tanks.game;

public enum Heading {
	NORTH(true),
	WEST(false),
	SOUTH(true),
	EAST(false);
	
	private boolean vertical;
	
	private Heading(boolean vertical) {
		this.vertical = vertical;
	}

	public boolean isVertical() {
		return vertical;
	}
	
	public boolean isHorisontal() {
		return !vertical;
	}

}
