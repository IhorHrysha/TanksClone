package com.grego.tanks.game;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import com.grego.tanks.game.mobile.Bullet;
import com.grego.tanks.game.mobile.Tank;
import com.grego.tanks.game.statics.Explosion;
import com.grego.tanks.game.statics.Tile;
import com.grego.tanks.main.Handler;


public abstract class Entity {

	public final EntityType type;
	
	protected float x,y;
	protected int width, height;
	protected int scale;
	
	protected Handler handler;
	
	protected Rectangle bounds;
	protected BufferedImage sprite;
	protected boolean active;
	
	//is entity walkthruoght
	protected boolean isWalkable;
	
	
	public Entity(EntityType type, Handler handler, float x, float y, int width, int height) {
		this.type = type;
		this.handler = handler;
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		scale = handler.getGame().getScale();
				
		bounds = new Rectangle(0, 0, width, height);
		
		active = true;
		
		isWalkable = false;
	}
	
	
	protected boolean hasIntersection(Entity e, float xOffset, float yOffset) {
		
		return e.getCollisionBounds(0f, 0f).intersects(getCollisionBounds(xOffset, yOffset));
	}
	
	public Rectangle getCollisionBounds(float xOffset, float yOffset) {
		return new Rectangle((int)(x+bounds.x+xOffset), (int)(y+bounds.y+yOffset), bounds.width, bounds.height);
	}
	
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
		bounds.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
		bounds.height = height;
	}

	public boolean isActive() {
		return active;
	}
	
	public void setActive(boolean active) {
		this.active = active;
	}
	
	public EntityType getType() {
		return type;
	}

	public boolean isWalkable() {
		return isWalkable;
	}
	
	public void setWalkable(boolean isWalkable) {
		this.isWalkable = isWalkable;
	}
	
	public abstract void update();
	
	public void render(Graphics2D g) {
		g.drawImage(sprite, (int)x, (int)y, width, height, null);
	}
	
}
