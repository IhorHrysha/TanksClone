package com.grego.tanks.utils;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.List;
import java.awt.Toolkit;
import java.awt.image.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Utils {

	public static BufferedImage resize(BufferedImage image, int width, int height) {
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);

		newImage.getGraphics().drawImage(image, 0, 0, width, height, null);

		//newImage = makeTransparentBackground(newImage,Color.black);
		
		return newImage;
	}

	public static Integer[][] levelParser(String filePath) {

		Integer[][] result = null;

		try (BufferedReader reader = new BufferedReader(new FileReader(new File(filePath)))) {

			ArrayList<Integer[]> lvlLines = new ArrayList<Integer[]>();

			String line = null;

			while ((line = reader.readLine()) != null) {
				lvlLines.add(str2int_arrays(line.split(" ")));
			}

			int rowNum = lvlLines.size();
			int colNum = lvlLines.get(0).length;

			result = new Integer[rowNum][colNum];

			for (int i = 0; i < rowNum; i++) {
				result[i] = lvlLines.get(i);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static final Integer[] str2int_arrays(String[] sArr) {
		Integer[] result = new Integer[sArr.length];

		for (int i = 0; i < sArr.length; i++) {
			result[i] = Integer.parseInt(sArr[i]);
		}
		return result;
	}

	public static Image makeColorTransparent(BufferedImage im, final Color color) {
		ImageFilter filter = new RGBImageFilter() {

			// the color we are looking for... Alpha bits are set to opaque
			public int markerRGB = color.getRGB() | 0xFF000000;

			public final int filterRGB(int x, int y, int rgb) {
				if ((rgb | 0xFF000000) == markerRGB) {
					// Mark the alpha bits as zero - transparent
					return 0x00FFFFFF & rgb;
				} else {
					// nothing to do
					return rgb;
				}
			}
		};

		ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
		return Toolkit.getDefaultToolkit().createImage(ip);
	}

	/**
	 * Converts a given Image into a BufferedImage
	 *
	 * @param img
	 *            The Image to be converted
	 * @return The converted BufferedImage
	 */
	public static BufferedImage toBufferedImage(Image img) {
		if (img instanceof BufferedImage) {
			return (BufferedImage) img;
		}

		// Create a buffered image with transparency
		BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

		// Draw the image on to the buffered image
		Graphics2D bGr = bimage.createGraphics();
		bGr.drawImage(img, 0, 0, null);
		bGr.dispose();

		// Return the buffered image
		return bimage;
	}

	public static BufferedImage makeTransparentBackground(BufferedImage im, final Color color) {
		Image img = makeColorTransparent(im, color);

		return toBufferedImage(img);
	}

}
