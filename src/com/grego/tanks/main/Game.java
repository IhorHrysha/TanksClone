package com.grego.tanks.main;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferStrategy;

import com.grego.tanks.OI.Input;
import com.grego.tanks.display.Display;
import com.grego.tanks.graphics.Assets;
import com.grego.tanks.graphics.TextureAtlas;
import com.grego.tanks.states.GameState;
import com.grego.tanks.states.MenuState;
import com.grego.tanks.states.State;
import com.grego.tanks.utils.Time;

public class Game implements Runnable {

	//Constant		
	public static final Color CLEAR_COLOR = Color.black;
	public static final int NUM_BUFFERS = 3;

	public static final float UPDATE_RATE = 60f;
	public static final float UPDATE_INTERVAL = Time.SECOND/UPDATE_RATE;
	public static final float IDLE_TIME = 1;
	
	//Sizes
	private int scale;
	private int tileSize;
	private int tankSize;
	
	private int width, height;
	private String title;
		
	//Threads
	private boolean running;
	private Thread	gameThread;
	
	//GFX
	private Display display;
	private Graphics2D g;
	private BufferStrategy bs;
	
	
	//States
	private State gameState;
	private State menuState;
	
	//OI
	private Input input;
	
	//Handler
	private Handler handler;
				
	public Game(String title) {
			
		this.title = title;
		
		running = false;		
	}

	private void init() {
		
		scale = 2;
		tileSize = 8;//min cell 8x8
		tankSize = tileSize*2;//tank 16x16
		
		//classic 13x13 + borders in tiles
		width = (26+6)*tileSize*scale;
		height = (26+4)*tileSize*scale;
		
		display = new Display(title, width, height);
				
		input = new Input();
		display.getWindow().add(input);
				
		handler = new Handler(this);
		
		Assets.init(handler);

		playGame();
		
		//menuState = new MenuState(handler);
				
	}
	
	public void playGame() {
		gameState = new GameState(handler);
		State.setCurrentState(gameState);
	}
	
	
	
	public synchronized void start() {
		
		if(running)
			return;
		
		running = true;
		
		//starts new thread from itself
		gameThread = new Thread(this);
		gameThread.start();
	}

	public synchronized void stop() {
		
		if(!running)
			return;
		
		running = false;
		
		try {
			gameThread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
	}

	private void update() {
		
		if(State.getCurrentState() != null)
			State.getCurrentState().update();
	}

	private void render() {
		
		//bufferization
		bs = display.getContent().getBufferStrategy();
		
		if (bs== null) {
			display.getContent().createBufferStrategy(NUM_BUFFERS);
			return;
		}
		
		g=(Graphics2D) bs.getDrawGraphics();
		//ANTIALIASING - ON
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		//Clear Screen
		g.setColor(CLEAR_COLOR);
		g.fillRect(0, 0, width, height);
		
		//Draw Here!
		
		if(State.getCurrentState() != null)
			State.getCurrentState().render(g);
				
			
		//End Drawing
		
		bs.show();
		g.dispose();
		
	}

	@Override
	public void run() {
		
		init();
		
		//statistics
		int fps = 0;
		int upd = 0;
		int updl = 0;
		long statsTimeCounter = 0;
		
		float delta = 0;
		
		//of iteration
		long lastTime = Time.get();
		
		while(running) {
			
			//diff btw last iteration
			long now = Time.get();
			long elapsedTime = now - lastTime;
			lastTime = now;
			
			statsTimeCounter += elapsedTime;
			
			delta += elapsedTime/UPDATE_INTERVAL;
			
			boolean render = false;
			
			//change game logic
			while(delta>1) {
				update();
				upd++;
				delta--;
				
				if (render) {
					updl++;
				}else {
					render = true;
				}
								
			}
			
			//show image
			if (render) {
				render();
				fps++;
			} else {
				//nothing to render
				try {
					Thread.sleep((long) IDLE_TIME);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			//refresh statistics every second
			if(statsTimeCounter>=Time.SECOND) {
				display.getWindow().setTitle(title + " || Fps: " + fps + " | Upd: " + upd + " | Updl: " + updl + " ||" );
				fps = 0;
				upd = 0;
				updl = 0;
				statsTimeCounter-=Time.SECOND;
			}
		}
			

	}


	//GETTERS/SETTERS
	
	public int getWidth() {
		return width;
	}

	public int getScale() {
		return scale;
	}

	public void setScale(int scale) {
		this.scale = scale;
	}


	public int getTileSize() {
		return tileSize;
	}

	public void setTileSize(int tileSize) {
		this.tileSize = tileSize;
	}

	public int getTankSize() {
		return tankSize;
	}

	public void setTankSize(int tankSize) {
		this.tankSize = tankSize;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	
	public Input getInput() {
		return input;
	}

	

}
