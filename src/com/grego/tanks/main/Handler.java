package com.grego.tanks.main;

import com.grego.tanks.OI.Input;
import com.grego.tanks.game.EntityManager;
import com.grego.tanks.level.Level;

public class Handler {
	private Game game;
	private Level level;
	
	public Handler(Game game) {
		this.game = game;
	}

	public Input getInput() {
		return game.getInput();
	}
	
	public int getWidth() {
		return game.getWidth();
	}
	
	public int getHeight() {
		return game.getHeight();
	}

	public Game getGame() {
		return game;
	}

	public void setGame(Game game) {
		this.game = game;
	}

	public Level getLevel() {
		return level;
	}

	public void setLevel(Level level) {
		this.level = level;
	}
	
	public EntityManager getEntityManager() {
		if (level!=null) {
			return level.getEntityManager();
		}
		
		return null;
		
	}
}
