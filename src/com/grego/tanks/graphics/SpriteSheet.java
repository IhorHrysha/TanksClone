package com.grego.tanks.graphics;

import java.awt.image.BufferedImage;

public class SpriteSheet {
	
	private BufferedImage sheet;
	private int spriteCount;
	private int scale;
	private int sprinesInWidth;
	
	public SpriteSheet(BufferedImage sheet, int spriteCount, int scale) {
		this.sheet = sheet;
		this.spriteCount = spriteCount;
		this.scale = scale;
		
		this.sprinesInWidth = sheet.getWidth()/scale;
		
	}
	
	public BufferedImage getSprite(int index) {
		
		index = index % spriteCount;
		
		int x = index % sprinesInWidth * scale;
		int y = index / sprinesInWidth * scale;
		
		return sheet.getSubimage(x, y, scale, scale);
	}

}
