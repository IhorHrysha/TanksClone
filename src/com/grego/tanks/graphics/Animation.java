package com.grego.tanks.graphics;

import java.awt.image.BufferedImage;

public class Animation {
	private int speed, index;
	private long lastTime, timer;
	boolean playedOnce;
	private BufferedImage [] frames;
	
	
	public Animation(int speed, BufferedImage[] frames) {
		this.speed = speed;
		this.frames = frames;
				
		index = 0;
		timer = 0;
		lastTime=System.currentTimeMillis();
		playedOnce = false;
	}

	public void update() {

		
		timer+=System.currentTimeMillis()-lastTime;
		lastTime = System.currentTimeMillis();
		
		if(timer>speed) {
			index++;
			timer = 0;
			if (index>=frames.length) {
				playedOnce = true;
				index = 0;
			}
				
		}
	}
	
	public BufferedImage getCurrentFrame() {
		return frames[index];
	}
	
	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public BufferedImage[] getFrames() {
		return frames;
	}

	public void setFrames(BufferedImage[] frames) {
		this.frames = frames;
	}

	public boolean isPlayedOnce() {
		return playedOnce;
	}
	
}
