package com.grego.tanks.graphics;

import java.awt.image.BufferedImage;

import com.grego.tanks.utils.ResourceLoader;

public class TextureAtlas {
	
	BufferedImage image;
	
	public TextureAtlas(String imageName) {
		image = ResourceLoader.loadImage(imageName);
	}
	
	public TextureAtlas(BufferedImage image) {
		this.image = image;
	}
	
	public void setImage(BufferedImage image) {
		this.image = image;
	}
	
	public BufferedImage cut(int x, int y, int w, int h) {
		return image.getSubimage(x, y, w, h);
	}
	
	public BufferedImage cutAndScale(int x, int y, int w, int h) {
		return image.getSubimage(x, y, w, h);
	}
	
	public BufferedImage scaledCut(int scale, float x, float y, float w, float h) {
		return cut((int)(x*scale), (int)(y*scale), (int)(w*scale), (int)(h*scale));
	}
	
}
