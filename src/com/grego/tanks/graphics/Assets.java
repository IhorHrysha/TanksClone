package com.grego.tanks.graphics;

import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;

import com.grego.tanks.game.Heading;
import com.grego.tanks.game.mobile.Tank;
import com.grego.tanks.game.mobile.TankColor;
import com.grego.tanks.game.statics.TileType;
import com.grego.tanks.main.Handler;

public class Assets {
	
	public static final int SPRITE_SIZE = 16;
	public static final String ATLAS_FILE_NAME = "texture_atlas.png";
			
	public static Map<Heading, BufferedImage> bulletImageMap;
	
	public static Map<Integer, BufferedImage> tankImageMap;
		
	public static Map<TileType, BufferedImage> tilesImageMap;
	
	public static BufferedImage eagleAlive;
	public static BufferedImage eagleDead;
	
	public static BufferedImage border;
	public static BufferedImage[] explosionFrames;
	
	public static void init(Handler handler) {
				
		int tileSize = SPRITE_SIZE/2;
		
		TextureAtlas atlas = new TextureAtlas(ATLAS_FILE_NAME);
						
		
		//Tanks
		tankImageMap = new HashMap<Integer, BufferedImage>();
		
		fillTankTextures(atlas);		
		
		
		//Bullets
		bulletImageMap = new HashMap<Heading, BufferedImage>();
		TextureAtlas bulletAtlas= new TextureAtlas(atlas.scaledCut(SPRITE_SIZE, 20, 6, 2, 1));
		
		for(Heading h:Heading.values()) {
			
			BufferedImage textureImage = bulletAtlas.scaledCut(tileSize, h.ordinal(), 0.5f, 1, 1);
			bulletImageMap.put(h, textureImage);
		}
		
		
		//Tiles
		tilesImageMap = new HashMap<TileType, BufferedImage>();
		tilesImageMap.put(TileType.BRICK,atlas.scaledCut(tileSize,32, 0, 1, 1));
		tilesImageMap.put(TileType.METAL,atlas.scaledCut(tileSize,32, 2, 1, 1));
		tilesImageMap.put(TileType.WATER, atlas.scaledCut(tileSize,32, 4 * 1, 1, 1));
		tilesImageMap.put(TileType.GRASS,atlas.scaledCut(tileSize,34, 4, 1, 1));
		tilesImageMap.put(TileType.ICE,atlas.scaledCut(tileSize,36, 4, 1, 1));
		tilesImageMap.put(TileType.EMPTY,atlas.scaledCut(tileSize,36, 6, 1, 1));
		
		//Eagle
		eagleAlive = atlas.scaledCut(SPRITE_SIZE,19, 2, 1, 1);
		eagleDead = atlas.scaledCut(SPRITE_SIZE,20, 2, 1, 1);
		
		border = atlas.scaledCut(SPRITE_SIZE,23, 0, 1, 1);
		
		
		//Explosion
		explosionFrames = new BufferedImage[3];
		explosionFrames[0] = atlas.scaledCut(SPRITE_SIZE,16, 8, 1, 1);
		explosionFrames[1] = atlas.scaledCut(SPRITE_SIZE,17, 8, 1, 1);
		explosionFrames[2] = atlas.scaledCut(SPRITE_SIZE,18, 8, 1, 1);
	}
	
	public static BufferedImage getTileTexture(TileType tileType) {
		return tilesImageMap.get(tileType);
	}
	
	private static void fillTankTextures(TextureAtlas atlas) {
		
		// 8 tank type = 4 stars*2 enemy/friend
		BufferedImage textureImage;
		int xShift, yShift, hash, xPos, yPos;

		for (TankColor c : TankColor.values()) {

			xShift = (int) c.getTexturePosition().getX();
			yShift = (int) c.getTexturePosition().getY();
			
			for (yPos = 0; yPos < 7; yPos++) {

				// 4 directions*2 animations
				for (Heading h : Heading.values()) {

					xPos = h.ordinal() * 2;
					hash = c.ordinal() * 100 + yPos * 10 + xPos;

					textureImage = atlas.scaledCut(SPRITE_SIZE, xPos+xShift, yPos+yShift, 1, 1);
					tankImageMap.put(hash, textureImage);
					
					//animation
					hash++; xPos++;
					textureImage = atlas.scaledCut(SPRITE_SIZE, xPos+xShift, yPos+yShift, 1, 1);
					tankImageMap.put(hash, textureImage);
					
				}
			}
		}

	}

	
	public static BufferedImage getTankTexture (Tank tank, int animationFrame) {
		
		int hash = tank.getColor().ordinal()*100+(tank.getStarsCount()+(tank.isEnemy()?4:0))*10+tank.getHeading().ordinal()*2+animationFrame;  
		
		return tankImageMap.get(hash);
	}
	
	
}

