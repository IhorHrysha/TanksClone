package com.grego.tanks.states;

import java.awt.Graphics2D;

import com.grego.tanks.main.Handler;
import com.grego.tanks.states.State;

public abstract class State {
	private static State currentState = null;
	private static int currentLevel = 1;
	
	protected Handler handler;
	
	public State(Handler handler) {
		this.handler = handler;		
	}
	
	public static State getCurrentState() {
		return currentState;
	}

	public static void setCurrentState(State currentState) {
		State.currentState = currentState;
	}
	
	public abstract void update();
	
	public abstract void render(Graphics2D g);
}
