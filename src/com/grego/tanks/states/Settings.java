package com.grego.tanks.states;

public class Settings {
	private static Settings instance;
	
	private boolean multyPlayer;
	private int totalScore;
	private int scorePlayer1, scorePlayer2;
	private int currentLevel;
	private int player1Lives, player2Lives;
	
	private Settings() {
		totalScore = 0;
		scorePlayer1 = 0;scorePlayer2 = 0;
		currentLevel = 1;
	}
	
	public static Settings getInstance() {
		if (instance!=null)
			instance = new Settings();
		return instance;
	}
	
	public void nextLevel() {
		currentLevel++;
	}
	
	
	
}
