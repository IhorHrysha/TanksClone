package com.grego.tanks.states;

import java.awt.Graphics2D;

import com.grego.tanks.level.Level;
import com.grego.tanks.main.Handler;

public class GameState extends State {
		
	private Level level;
	
	public GameState(Handler handler) {
		super(handler);
		level = new Level(handler);
		
		handler.setLevel(level);
	}

	@Override
	public void update() {
		level.update();
		
		//if level completed
		//move to statictics stage
		
		//if(level.isGameOver())
		//	Settings.getInstance().nextLevel();
	}

	@Override
	public void render(Graphics2D g) {
		level.render(g);
	}

}
