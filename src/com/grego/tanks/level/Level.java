package com.grego.tanks.level;

import java.awt.Graphics2D;

import com.grego.tanks.game.EntityManager;
import com.grego.tanks.game.mobile.Enemy;
import com.grego.tanks.game.mobile.Player;
import com.grego.tanks.game.statics.Border;
import com.grego.tanks.game.statics.Eagle;
import com.grego.tanks.game.statics.Tile;
import com.grego.tanks.game.statics.TileType;
import com.grego.tanks.main.Handler;
import com.grego.tanks.utils.Utils;

public class Level {
		
	private Integer[][] tileMap;
	
	private Handler handler;
	
	private EntityManager entityManager;
	
	int tileSize, scale;
	
	boolean gameOver;

	public Level(Handler handler) {
				
		this.handler = handler;
				
		entityManager = new EntityManager(handler);
		
		loadLevel(1);
				
	}
	
	private void loadLevel(int levelNumer) {
		
		tileMap = Utils.levelParser("res/level"+levelNumer+".lvl");
						
		tileSize = handler.getGame().getTileSize();
		scale = handler.getGame().getScale();
				
		for (int y = 0; y < tileMap.length; y++) {			
			for (int x = 0; x < tileMap[y].length; x++) {
				
				TileType tileType = TileType.fromNumeric(tileMap[y][x]);
				
				if (tileType == TileType.EMPTY)
					continue;
				
				Tile tile = new Tile(handler, (x+2)*tileSize*scale, (y+2)*tileSize*scale);
				
				tile.setTileType(tileType);
				entityManager.addEntity(tile);
				
			}
		}
		
		entityManager.addEntity(new Eagle(handler, 14*tileSize*scale, 26*tileSize*scale));
		entityManager.addEntity(new Player(handler,Player.FIRST));
		entityManager.addEntity(new Player(handler,Player.SECOND));
		
		entityManager.addEntity(new Enemy(handler,2*tileSize*scale,2*tileSize*scale,0));
		entityManager.addEntity(new Enemy(handler,14*tileSize*scale,2*tileSize*scale,1));
		entityManager.addEntity(new Enemy(handler,26*tileSize*scale,2*tileSize*scale,2));
		
		//borders
		entityManager.addEntity(new Border(handler, 0*tileSize*scale, 0*tileSize*scale, 2*tileSize*scale, 30*tileSize*scale) );
		entityManager.addEntity(new Border(handler, 2*tileSize*scale, 0*tileSize*scale, 26*tileSize*scale, 2*tileSize*scale) );
		entityManager.addEntity(new Border(handler, 28*tileSize*scale, 0*tileSize*scale, 6*tileSize*scale, 30*tileSize*scale) );
		entityManager.addEntity(new Border(handler, 2*tileSize*scale, 28*tileSize*scale, 26*tileSize*scale, 2*tileSize*scale) );
		
	}
	
	public void update() {

		entityManager.update();
		
	}
	
	public void render(Graphics2D g) {
	
		entityManager.render(g);
		
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
}

