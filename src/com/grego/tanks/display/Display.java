package com.grego.tanks.display;

import java.awt.Canvas;
import java.awt.Dimension;

import javax.swing.JFrame;

public class Display {
	
	private JFrame window;
	private Canvas content;

	private String title;
	private int width, height;
	
	public Display(String title, int width, int height) {

		this.title = title;
		this.width = width;
		this.height = height;
		
		create();

	}

	public void create() {

		window = new JFrame(title);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.setResizable(false);		
		window.setLocationRelativeTo(null);
		window.setVisible(true);
		
		content = new Canvas();
		content.setPreferredSize(new Dimension(width, height));
		content.setMaximumSize(new Dimension(width, height));
		content.setMinimumSize(new Dimension(width, height));
		content.setFocusable(false);		
		
		window.add(content);		
		window.pack();		

	}

	public Canvas getContent() {
		return content;
	}
	
	public JFrame getWindow() {
		return window;
	}
	
	public String getTitle() {
		return title;
	}
		

}